ARG docker_base_image_url
FROM $docker_base_image_url

RUN apt update -y && apt install curl --no-install-recommends -y
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

WORKDIR /app
COPY app/get_xml_notices.py /app/get_xml_notices.py

EXPOSE 7861

CMD ["python3", "/app/get_xml_notices.py"]
