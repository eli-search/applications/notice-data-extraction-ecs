import os
import uuid
from typing import List

import boto3

SQS_MAX_BATCH_SIZE = 10


class SqsInsertionException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class S3ReadException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class MissingEnvironmentVariableException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


def sqs_client():
    return boto3.client('sqs')


def s3_client():
    return boto3.client("s3")


def get_env_var_value(env_variable_name: str) -> str:
    try:
        return os.environ[env_variable_name]
    except KeyError as err:
        raise MissingEnvironmentVariableException(
                f"Missing environment variable '{env_variable_name}', process stopped.") from err


def insert_in_queue(entries: List[dict], queue_url: str) -> None:
    try:
        sqs_client().send_message_batch(QueueUrl=queue_url, Entries=entries)
    except Exception as err:
        raise SqsInsertionException(f"Error while inserting in the SQS queue: {err}") from err


def insert_batches_in_queue(resources: List[str], queue_url: str, bucket: str) -> None:
    batches = [resources[x:x + SQS_MAX_BATCH_SIZE] for x in range(0, len(resources), SQS_MAX_BATCH_SIZE)]
    print(f'Inserting {len(resources)} in SQS queue : {queue_url}')
    for batch in batches:
        entries = []
        for resource_id in batch:
            entry = {'Id': str(uuid.uuid4()), 'MessageBody': f"{bucket}#{resource_id}"}
            entries.append(entry)
        insert_in_queue(entries, queue_url)


def get_s3_objects_list(bucket: str, prefix: str = '') -> List[str]:
    paths = list()

    try:
        s3 = s3_client()
        paginator = s3.get_paginator('list_objects_v2')
        pages = paginator.paginate(Bucket=bucket, Prefix=prefix)

        print(f'Started retrieving objects from S3 with pagination')
        page_count = 1
        for page in pages:
            print(f'Processing page {page_count}')
            s3_objects = page.get('Contents', list())
            page_object_count = 0
            for s3_object in s3_objects:
                paths.append(s3_object['Key'])
                page_object_count += 1
            print(f'Retrieved {page_object_count} from page {page_count}')
            page_count += 1
        print(f"Finished retrieving {len(paths)} S3 objects paths.")
        return paths
    except Exception as err:
        raise S3ReadException(f"Failed to read S3 object: {err}") from err


input_bucket = get_env_var_value("INPUT_BUCKET")
queue_url = get_env_var_value("QUEUE_URL")
print(f'Method get_xml_notices triggered with parameters : {input_bucket}, {queue_url}')
processed_count = 0
try:
    print("Started retrieving XML contract notices paths.")
    contract_notices = get_s3_objects_list(bucket=input_bucket,
                                           prefix="resource_type=contract_notice/format=xml/")
    print(f"Found {len(contract_notices)} XML contract notices in S3")
    insert_batches_in_queue(resources=contract_notices, queue_url=queue_url, bucket=input_bucket)
    processed_count += len(contract_notices)

    print("Started retrieving XML prior information notices paths.")
    prior_information_notices = get_s3_objects_list(bucket=input_bucket,
                                                    prefix="resource_type=prior_information_notice/format=xml/")
    print(f"Found {len(prior_information_notices)} XML prior information notices in S3")
    insert_batches_in_queue(resources=prior_information_notices, queue_url=queue_url, bucket=input_bucket)
    processed_count += len(prior_information_notices)

    print("Started retrieving XML contract award notices paths.")
    contract_award_notices = get_s3_objects_list(bucket=input_bucket,
                                                 prefix="resource_type=contract_award_notice/format=xml/")
    print(f"Found {len(contract_award_notices)} XML contract award notices in S3")
    insert_batches_in_queue(resources=contract_award_notices, queue_url=queue_url, bucket=input_bucket)
    processed_count += len(contract_award_notices)

    print("Started retrieving XML change notices paths.")
    change_notices = get_s3_objects_list(bucket=input_bucket,
                                         prefix="resource_type=change_notice/format=xml/")
    print(f"Found {len(change_notices)} XML change notices in S3")
    insert_batches_in_queue(resources=change_notices, queue_url=queue_url, bucket=input_bucket)
    processed_count += len(change_notices)

    print(f"{processed_count} notices added to transformation queue.")
except Exception as err:
    print(f'Error during the retrieval of notices from S3 : {err}')
    raise err
